<?php

class Helper
{
	public function _generateToken()
    {
        return md5(rand(9999,1)."-".time());
    }
    
    public function _generateRandomDateTime($start_date, $end_date)
	{
	    // Convert to timetamps
	    $min = strtotime($start_date);
	    $max = strtotime($end_date);

	    // Generate random number using above bounds
	    $val = rand($min, $max);

	    // Convert back to desired date format
	    return date('Y-m-d H:i', $val);
	
	}

	public function _convertToDate($date,$format){
		$date = new DateTime($date,new DateTimeZone("Asia/Hong_Kong"));
		return $date->format($format);
	}
    public function _addHours($date,$hours){
        $now = new DateTime($date,new DateTimeZone("Asia/Hong_Kong")); //current date/time
        $now->add(new DateInterval("PT{$hours}H"));
        $new_time = $now->format('M d, Y h:i A');
        return $new_time;
    }
	public function _paginate($data,$page,$limit){

		$paginator =  new PaginatorModel(
				array(
					"data"=>$data,
					"limit"=>$limit,
					"page"=>$page
				)
		);
		$page = $paginator->getPaginate();
		return $page->items;
	}
    public function _paginateArray($data,$page,$limit){

        $paginator =  new PaginatorArray(
                array(
                    "data"=>$data,
                    "limit"=>$limit,
                    "page"=>$page
                )
        );
        $page = $paginator->getPaginate();
        return $page->items;
    }

	public function _getLastPage($data,$limit){
		
		return ceil(sizeof($data) / $limit);	
	}
	public function _echoJson($status,$message,$data = []){
		$result = array(
				"status"=>$status,
				"message"=>$message,
				"data"=>$data,
                "messages"=>$message
		);
		echo json_encode($result);
		die;
	}
}








?>