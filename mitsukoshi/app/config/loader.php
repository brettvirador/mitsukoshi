<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */

$loader->registerClasses(
    array(
        "Helper"         => "helper/helper.php"
    )
);
$loader->registerDirs(
    array(
        $config->application->controllersDir,
        $config->application->modelsDir,
        $config->application->pluginsDir
    )
)->register();
/*
"PHP": {
            "php": "/Applications/MAMP/bin/php/php5.5.3/bin/php",
            "codeintel_scan_extra_dir": [],
            "codeintel_scan_files_in_project": true,
            "codeintel_max_recursive_dir_depth": 15,
            "codeintel_scan_exclude_dir":["/Applications/MAMP/bin/php/php5.5.3/"],
            
        }
*/