<?php

defined('APP_PATH') || define('APP_PATH', realpath('.'));


$env = 'default';
if(@$_SERVER['HTTP_HOST'] == 'localhost')
{
    $env = 'localhost';
}
else
{
    $env = 'devops';
}   


$baseuri = array();
$baseuri['localhost'] = 'http://localhost/workflow_management_system/mitsukoshi/';


$database['localhost'] = array(
        'adapter'     => 'Mysql',
        'host'        => 'localhost',
        'username'    => 'root',
        'password'    => '',
        'dbname'      => 'mitsukoshi',
        'charset'     => 'utf8',
    );

//set constant for base uri and image uri
define("BASE_URI",$baseuri['localhost']);

return new \Phalcon\Config(array(
    'database' => $database['localhost'],
    'application' => array(
        'controllersDir' => APP_PATH . '/app/controllers/',
        'modelsDir'      => APP_PATH . '/app/models/',
        'migrationsDir'  => APP_PATH . '/app/migrations/',
        'viewsDir'       => APP_PATH . '/app/views/',
        'pluginsDir'     => APP_PATH . '/app/plugins/',
        'libraryDir'     => APP_PATH . '/app/library/',
        'cacheDir'       => APP_PATH . '/app/cache/',
        'baseUri'        => $baseuri['localhost'],
    )
));
