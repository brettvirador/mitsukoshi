<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as Assets;


class CssLoader extends Plugin
{
	
	public function addCss($collection,$path,$local = true)
	{
		$css = $this->assets->collection($collection);
		$css->addCss($path,$local);
	}

	public function beforeDispatch(Event $event , Dispatcher $dispatcher)
	{
		$controller_name = $dispatcher->getControllerName();
		$action_name = $dispatcher->getActionName();
		
		switch ($controller_name) {
		
		}
	}
}