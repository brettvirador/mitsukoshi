<?php

use Phalcon\Acl;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Assets\Manager as Assets;


class JsLoader extends Plugin
{	

	
	public function outputGlobalHeaderJs($collection = "global_header_js")
	{

	}

	public function outputGlobalFooterJs($collection = 'global_footer_js')
	{
				
	}



	public function addJs($collection,$path,$local=true)
	{
		$scripts = $this->assets->collection($collection);
		$scripts->addJs($path,$local);
	}

	public function beforeDispatch(Event $event , Dispatcher $dispatcher)
	{
		$controller_name = $dispatcher->getControllerName();
		$action_name = $dispatcher->getActionName();
		switch($controller_name)
		{
			case "index":
				$this->addJs('global_footer_js','js/print_div.js');
			break;
		}

	}
}